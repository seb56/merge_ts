#ifndef _XLC
#include <features.h> 
#endif

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <sys/time.h>
#include <sys/times.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/file.h>
#include <sys/resource.h>
#include <sys/signal.h>

#ifndef _XLC
#include <sys/syscall.h>
#endif

#include "defs.h"
