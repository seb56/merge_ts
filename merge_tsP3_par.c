#include        "include.h"
#include        "structure.h"
#include        "function.h"
#include <unistd.h>
#include "mpi.h"

float float_swap(char *);
#define JJ 3

int main(int ac, char **av)
{

	FILE *fpr, *fopfile();
	struct tsheader tshead;
	struct tsheader_procP3 *tshead_pP3;
	int i, j, iy, it;
	int nfiles;
	float *v1, *v1s, *v2;
	int v1s_off, v2_off;
	int *v1_lens, *v1s_displs;

	int v1_last_idx;

	int k;
	float max, amax;

	int min_ix0, min_iy0, min_iz0, min_it0;

	int swap_bytes = 0;

	off_t head_off;

	int fdr, fdw, *fdrs;
	int fi;

	char str[512];
	char *filebuf;
	char **infile;
	char *ifile;

	char filelist[256];
	char outfile[256];
	char tmp[128];
	char cwd[128];


	int rank,size ;

	MPI_Status status;
	MPI_Request request;
	MPI_File fdw_mpi;

	double starttime,endtime, preptime, writetime, readtime, gathertime, preptimeCul=0, readtimeCul=0, gathertimeCul=0, writetimeCul=0;


	MPI_Init(&ac, &av);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

//	group_size = JJ;
//	num_groups = size / group_size;
//	group_id = rank / group_size ;
//	group_rank = rank % group_size;

	// if size=6, numgroups=2 . rank 0...2 will be on group_id 0, rank 3..5 will be on group_id 1.


	if (rank ==0) fprintf(stderr,"size=%d\n",size);

	if(getcwd(cwd,sizeof(cwd)) != NULL) {
		fprintf(stdout, "Current working dir:%s\n",cwd);
	}
	else {
		fprintf(stderr,"getcwd() error");
		exit(-1);
	}


	starttime=MPI_Wtime();

	setpar(ac, av);
	mstpar("filelist","s",filelist);
	mstpar("outfile","s",outfile);
	mstpar("nfiles","d",&nfiles);
	getpar("swap_bytes","d",&swap_bytes);
	endpar();

	tshead_pP3 = (struct tsheader_procP3 *) check_malloc (nfiles*sizeof(struct tsheader_procP3)); //space for keeping header of all files (each processor timeslice headers)
	filebuf = (char *) check_malloc (256*nfiles*sizeof(char));
	infile = (char **) check_malloc (nfiles*sizeof(char *));


	strcpy(tmp, cwd);
	strcat(tmp,"/");
	strcat(tmp,filelist);
	strcpy(filelist,tmp);

	fpr = fopfile(filelist,"r");

	min_ix0 = 99999999;
	min_iy0 = 99999999;
	min_iz0 = 99999999;
	min_it0 = 99999999;



	//tsheader struct
	int tsheader_blocklengths[15]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
	MPI_Datatype tsheader_types[15]={MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT};
	MPI_Datatype tsheader_mpi;


	MPI_Aint tsheader_disp[15];
	MPI_Get_address(&tshead.ix0, &tsheader_disp[0]);
	MPI_Get_address(&tshead.iy0, &tsheader_disp[1]);
	MPI_Get_address(&tshead.iz0, &tsheader_disp[2]);
	MPI_Get_address(&tshead.it0, &tsheader_disp[3]);
	MPI_Get_address(&tshead.nx, &tsheader_disp[4]);
	MPI_Get_address(&tshead.ny, &tsheader_disp[5]);
	MPI_Get_address(&tshead.nz, &tsheader_disp[6]);
	MPI_Get_address(&tshead.nt, &tsheader_disp[7]);
	MPI_Get_address(&tshead.dx, &tsheader_disp[8]);
	MPI_Get_address(&tshead.dy, &tsheader_disp[9]);
	MPI_Get_address(&tshead.dz, &tsheader_disp[10]);
	MPI_Get_address(&tshead.dt, &tsheader_disp[11]);
	MPI_Get_address(&tshead.modelrot, &tsheader_disp[12]);
	MPI_Get_address(&tshead.modellat, &tsheader_disp[13]);
	MPI_Get_address(&tshead.modellon, &tsheader_disp[14]);


	MPI_Aint base;
	base = tsheader_disp[0];
	for (i=0;i<15;i++) tsheader_disp[i] -= base;
	MPI_Type_create_struct(15,tsheader_blocklengths,tsheader_disp,tsheader_types, &tsheader_mpi);
	MPI_Type_commit(&tsheader_mpi);


	//tsheader_procP3 struct
	int tsheader_procP3_blocklengths[18]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
	MPI_Datatype tsheader_procP3_types[18]={MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT};
	MPI_Datatype tsheader_procP3_mpi;

	MPI_Aint tsheader_procP3_disp[18];
	MPI_Get_address(&tshead_pP3[0].ix0, &tsheader_procP3_disp[0]);
	MPI_Get_address(&tshead_pP3[0].iy0, &tsheader_procP3_disp[1]);
	MPI_Get_address(&tshead_pP3[0].iz0, &tsheader_procP3_disp[2]);
	MPI_Get_address(&tshead_pP3[0].it0, &tsheader_procP3_disp[3]);
	MPI_Get_address(&tshead_pP3[0].loc_nx, &tsheader_procP3_disp[4]);
	MPI_Get_address(&tshead_pP3[0].loc_ny, &tsheader_procP3_disp[5]);
	MPI_Get_address(&tshead_pP3[0].loc_nz, &tsheader_procP3_disp[6]);
	MPI_Get_address(&tshead_pP3[0].nx, &tsheader_procP3_disp[7]);
	MPI_Get_address(&tshead_pP3[0].ny, &tsheader_procP3_disp[8]);
	MPI_Get_address(&tshead_pP3[0].nz, &tsheader_procP3_disp[9]);
	MPI_Get_address(&tshead_pP3[0].nt, &tsheader_procP3_disp[10]);
	MPI_Get_address(&tshead_pP3[0].dx, &tsheader_procP3_disp[11]);
	MPI_Get_address(&tshead_pP3[0].dy, &tsheader_procP3_disp[12]);
	MPI_Get_address(&tshead_pP3[0].dz, &tsheader_procP3_disp[13]);
	MPI_Get_address(&tshead_pP3[0].dt, &tsheader_procP3_disp[14]);
	MPI_Get_address(&tshead_pP3[0].modelrot, &tsheader_procP3_disp[15]);
	MPI_Get_address(&tshead_pP3[0].modellat, &tsheader_procP3_disp[16]);
	MPI_Get_address(&tshead_pP3[0].modellon, &tsheader_procP3_disp[17]);

	//MPI_Aint base;
	base = tsheader_procP3_disp[0];
	for (i=0;i<18;i++) tsheader_procP3_disp[i] -= base;

	MPI_Type_create_struct(18,tsheader_procP3_blocklengths,tsheader_procP3_disp,tsheader_procP3_types, &tsheader_procP3_mpi);
	MPI_Type_commit(&tsheader_procP3_mpi);



	if (rank==0)
	{
		/* the following read part can be done in parallel too, but not necessary. */

		i = 0;
		while(fscanf(fpr,"%s",str) != EOF && i < nfiles)
		{
			infile[i] = filebuf + i*256;//pointing to 256-width slice of filebuf
			strcpy(infile[i],str);

			fprintf(stderr,"%d %s\n",i,infile[i]);

			strcpy(tmp, cwd);
			strcat(tmp,"/");
			strcat(tmp,infile[i]);
			strcpy(infile[i],tmp);

			fdr = opfile_ro(infile[i]);
			reed(fdr,&tshead_pP3[i],sizeof(struct tsheader_procP3));
			close(fdr);

			if(swap_bytes)
			{
				swap_in_place(1,(char *)(&tshead_pP3[i].ix0));
				swap_in_place(1,(char *)(&tshead_pP3[i].iy0));
				swap_in_place(1,(char *)(&tshead_pP3[i].iz0));
				swap_in_place(1,(char *)(&tshead_pP3[i].it0));
				swap_in_place(1,(char *)(&tshead_pP3[i].loc_nx));
				swap_in_place(1,(char *)(&tshead_pP3[i].loc_ny));
				swap_in_place(1,(char *)(&tshead_pP3[i].loc_nz));
				swap_in_place(1,(char *)(&tshead_pP3[i].nx));
				swap_in_place(1,(char *)(&tshead_pP3[i].ny));
				swap_in_place(1,(char *)(&tshead_pP3[i].nz));
				swap_in_place(1,(char *)(&tshead_pP3[i].nt));
				swap_in_place(1,(char *)(&tshead_pP3[i].dx));
				swap_in_place(1,(char *)(&tshead_pP3[i].dy));
				swap_in_place(1,(char *)(&tshead_pP3[i].dz));
				swap_in_place(1,(char *)(&tshead_pP3[i].dt));
				swap_in_place(1,(char *)(&tshead_pP3[i].modelrot));
				swap_in_place(1,(char *)(&tshead_pP3[i].modellat));
				swap_in_place(1,(char *)(&tshead_pP3[i].modellon));
			}

			if(tshead_pP3[i].ix0 < min_ix0)
				min_ix0 = tshead_pP3[i].ix0;
			if(tshead_pP3[i].iy0 < min_iy0)
				min_iy0 = tshead_pP3[i].iy0;
			if(tshead_pP3[i].iz0 < min_iz0)
				min_iz0 = tshead_pP3[i].iz0;
			if(tshead_pP3[i].it0 < min_it0)
				min_it0 = tshead_pP3[i].it0;
			i++;
		}

		if(i != nfiles)
		{
			fprintf(stderr,"(%d) entries in filelist != nfiles(%d), exiting ...\n",i,nfiles);
			exit(-1);
		}

		tshead.ix0 = min_ix0;
		tshead.iy0 = min_iy0;
		tshead.iz0 = min_iz0;
		tshead.it0 = min_it0;
		tshead.nx = tshead_pP3[0].nx;
		tshead.ny = tshead_pP3[0].ny;
		tshead.nz = tshead_pP3[0].nz;
		tshead.nt = tshead_pP3[0].nt;
		tshead.dx = tshead_pP3[0].dx;
		tshead.dy = tshead_pP3[0].dy;
		tshead.dz = tshead_pP3[0].dz;
		tshead.dt = tshead_pP3[0].dt;
		tshead.modelrot = tshead_pP3[0].modelrot;
		tshead.modellat = tshead_pP3[0].modellat;
		tshead.modellon = tshead_pP3[0].modellon;

//		fprintf(stderr,"ix0= %d\n",tshead.ix0);
//		fprintf(stderr,"iy0= %d\n",tshead.iy0);
//		fprintf(stderr,"nx= %d dx= %lg\n",tshead.nx,tshead.dx);
//		fprintf(stderr,"ny= %d dy= %lg\n",tshead.ny,tshead.dy);
//		fprintf(stderr,"nt= %d dt= %lg\n",tshead.nt,tshead.dt);
//		fflush(stderr);

	}

	//let everyone have info on tshead, tshead_pP3[i...nfiles] and input files
	MPI_Bcast(&tshead, 1, tsheader_mpi,0,MPI_COMM_WORLD);
	MPI_Bcast(&tshead_pP3[0], nfiles, tsheader_procP3_mpi,0,MPI_COMM_WORLD);
	MPI_Bcast(&filebuf[0], 256*nfiles, MPI_CHAR, 0, MPI_COMM_WORLD);


//	if (rank!=0) {
//		fprintf(stderr,"!! ix0= %d\n",tshead.ix0);
//		fprintf(stderr,"!! iy0= %d\n",tshead.iy0);
//		fprintf(stderr,"!! nx= %d dx= %lg\n",tshead.nx,tshead.dx);
//		fprintf(stderr,"!! ny= %d dy= %lg\n",tshead.ny,tshead.dy);
//		fprintf(stderr,"!!nt= %d dt= %lg\n",tshead.nt,tshead.dt);
//
//	}


	v1 = (float *) check_malloc (JJ*tshead.nx*tshead.ny*sizeof(float)); //read buffer owned/used by each process. Size is allocated very generously here.
	v1s = (float *) check_malloc (JJ*tshead.nx*tshead.ny*sizeof(float)); //read buffer owned by write process (rank 0) for gathering v1's from all processes
	v2 = (float *) check_malloc (JJ*tshead.nx*tshead.ny*sizeof(float)); //write buffer that keeps ordered contiguous data for each timeslice ready for writing.

	v1_lens = (int*) check_malloc(size*sizeof(int)); //writer process will gather v1's from all processes and concatenate them. this keeps the length of each v1
	v1s_displs = (int*) check_malloc(size*sizeof(int)); //used for concatenating v1's during gathering process

	for(iy=0;iy<JJ*tshead.nx*tshead.ny;iy++) {
		v1[iy] = 0;
		v1s[iy] = 0;
		v2[iy] = 0;

	}
//	for(iy=tshead.nx*tshead.ny;iy<tshead.nt*tshead.nx*tshead.ny;iy++) {
//			v1[iy] = 0;
//	}

	//add the path to the file name. Not needed if outfile is in the current directory.
	strcpy(tmp, cwd);
	strcat(tmp,"/");
	strcat(tmp,outfile);
	strcpy(outfile,tmp);


	preptime=MPI_Wtime();

	if (rank==0) {

		fdw = croptrfile(outfile);
		rite(fdw,&tshead,sizeof(struct tsheader));;


		for(it=0;it<tshead.nt;it++) {
			rite(fdw,&v1[0],JJ*tshead.nx*tshead.ny*sizeof(float)); //creating zero-filled file. Could do in parallel, but not needed.
		}


		lseek(fdw,sizeof(struct tsheader),SEEK_SET);

		close(fdw);
	}


	if (rank ==0) fprintf(stderr,"Creating zero-filled file took %f seconds\n", MPI_Wtime()-preptime);


	head_off = sizeof(struct tsheader);

	amax = -1;

	int nfiles_per_proc = nfiles/size;

	fdrs=(int *)check_malloc(sizeof(int)*nfiles_per_proc);

	for(i=nfiles_per_proc*rank;i<nfiles_per_proc*(rank+1);i++) {
		fi = i - nfiles_per_proc*rank;
		ifile = filebuf+i*256;
		fdrs[fi] = opfile_ro(ifile);
		//lseek(fdrs[fi],sizeof(struct tsheader_procP3)+(it*read_width)*sizeof(float),SEEK_SET); //for each input file, skip the header part.
		lseek(fdrs[fi],sizeof(struct tsheader_procP3),SEEK_SET); //for each input file, skip the header part.
	}

	MPI_File_open(MPI_COMM_WORLD,outfile, MPI_MODE_CREATE|MPI_MODE_RDWR,MPI_INFO_NULL,&fdw_mpi);
	MPI_File_seek(fdw_mpi,head_off,MPI_SEEK_SET);

	preptimeCul += (MPI_Wtime()-starttime);

	for(it=0;it<tshead.nt;it++)
	{//for each time slice

	  v1_last_idx = 0;

	  if (rank==0) fprintf(stderr,"it=%d/%d\n",it, tshead.nt);

	  readtime = MPI_Wtime();
	  for(i=nfiles_per_proc*rank;i<nfiles_per_proc*(rank+1);i++)
	  {//each rank takes care of nfiles/size files.

		max = 0.0;
		int read_width = JJ*tshead_pP3[i].loc_nx*tshead_pP3[i].loc_ny; //width of each read

		if(tshead_pP3[i].loc_nx > 0 && tshead_pP3[i].loc_ny > 0 && tshead_pP3[i].loc_nz > 0)
		{
			//fprintf(stderr,"it=%d/%d rank=%d on file %d %s\n",it, tshead.nt, rank, i, ifile);
			//fflush(stderr);
			fi = i - nfiles_per_proc*rank;
			reed(fdrs[fi],&v1[v1_last_idx],read_width*sizeof(float)); //read read_width sized data

			if(swap_bytes) swap_in_place(read_width,(char *)(&v1[v1_last_idx])); //may not work. v1's index needs to be adjusted
			v1_last_idx += read_width;
		}//if
	  }//for nfiles

	  readtimeCul += (MPI_Wtime() - readtime);

	  //mpi_gather
	  //fprintf(stderr,"it=%d rank=%d v1_last_idx=%d v1[0]=%f v1[1]=%f v1[2]=%f...v1[%d]=%f\n",it,rank,v1_last_idx, v1[0],v1[1],v1[2],v1_last_idx, v1[v1_last_idx]);
	  gathertime = MPI_Wtime();
	  MPI_Gather(&v1_last_idx,1,MPI_INT,v1_lens,1,MPI_INT,0,MPI_COMM_WORLD); //rank 0 collects the length of v1's of all ranks. (they vary)

	  for (k=0;k<size;k++) v1s_displs[k]=0;
	  for (k=1;k<size;k++) v1s_displs[k]=v1s_displs[k-1]+v1_lens[k-1];//v1s_displs is used to concatenate v1's from all ranks

	  //gathering v1 of size v1_last_idx from each rank and make v1s (a concat'd collection of v1's). As v1's are of varying length, we use MPI_Gatherv
	  MPI_Gatherv(v1,v1_last_idx, MPI_FLOAT, v1s, v1_lens, v1s_displs, MPI_FLOAT,0, MPI_COMM_WORLD);
	  gathertimeCul += (MPI_Wtime()-gathertime);
/*
	  if (rank==0) {
		  char str[1000];
		  strcpy(str,"\0");
		  for (k=0;k<size;k++) {
			  sprintf(&str[strlen(str)],"##v1_lens[%d]=%d v1s_displs[%d]=%d ",k,v1_lens[k],k,v1s_displs[k]);
		  }
		  for (k=0;k<size;k++) {
			  sprintf(&str[strlen(str)], "v1s[%d]=%f v1s[%d]=%f v1s[%d]=%f ",v1s_displs[k],v1s[v1s_displs[k]],v1s_displs[k]+1,v1s[v1s_displs[k]+1],v1s_displs[k]+2, v1s[v1s_displs[k]+2]);
		  }
		  fprintf(stderr,"%s\n",str);

	  }
*/
	  //Re-arranging data from v1s and copy to v2.
	  if (rank==0)
	  {
		  writetime = MPI_Wtime();
		  if (it>0) MPI_Wait( &request, &status );

		  int v1_last_idx = 0;

		  for(i=0;i<nfiles;i++) //in v1s, we have nfiles blocks
		  {
			  if(tshead_pP3[i].loc_nx > 0 && tshead_pP3[i].loc_ny > 0 && tshead_pP3[i].loc_nz > 0)
			  {
				  for (j=0;j<JJ;j++)
				  {
					  for(iy=0;iy<tshead_pP3[i].loc_ny;iy++) //along y axis
					  {
						  v1s_off = (j*tshead_pP3[i].loc_ny+iy)*tshead_pP3[i].loc_nx;//+(it*tshead_pP3[i].loc_nx*tshead_pP3[i].loc_ny);
						  v2_off = j*tshead.nx*tshead.ny+(iy+tshead_pP3[i].iy0)*tshead.nx+(tshead_pP3[i].ix0); //inside nx*ny block, skip nx*iy (local index) of first point in y-axis

						  for (k=0;k<tshead_pP3[i].loc_nx;k++)
						  {
							  v2[v2_off+k]=v1s[v1_last_idx+v1s_off+k];
						  }
					  }
				  }
				  v1_last_idx += tshead_pP3[i].loc_nx*tshead_pP3[i].loc_ny*JJ; //Beginning of a new block
			  }//if
		  }//for i

//		  disp = head_off //size of header to skip
//				  + (off_t)((JJ*it)*tshead.nx*tshead.ny)*sizeof(float) ;//output file has JJ blocks of size nx*ny per each timeslice. skip all past timeslices

//		  MPI_File_write_at(fdw_mpi, disp, (void *)(&v2[0]),JJ*tshead.nx*tshead.ny, MPI_FLOAT, &status); //could use serial rite() function here. for future scaling, leave as it is.
//		  MPI_File_write(fdw_mpi,(void *)(&v2[0]),JJ*tshead.nx*tshead.ny, MPI_FLOAT, &status);
		  MPI_File_iwrite(fdw_mpi,(void *)(&v2[0]),JJ*tshead.nx*tshead.ny, MPI_FLOAT, &request);


		  for(iy=0;iy<JJ*tshead.nx*tshead.ny;iy++)
		  {//reset the buffers.
			  v1[iy] = 0;
			  v1s[iy] = 0;
			  v2[iy] = 0;
		  }
		  writetimeCul += (MPI_Wtime()-writetime);
	  }//if rank==0

	}//for nt
		/*
fprintf(stderr,"max=%13.5e\n",max);
if(max>amax) amax = max;
		 */
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_File_close(&fdw_mpi);
	for(i=nfiles_per_proc*rank;i<nfiles_per_proc*(rank+1);i++) {
		close(fdrs[fi]);
	}

	endtime = MPI_Wtime();

	if (rank ==0) fprintf(stderr,"Writing files took %f seconds (prep %f read %f gather %f write %f secs) \n",endtime-starttime,preptimeCul, readtimeCul, gathertimeCul,writetimeCul);
	/*
fprintf(stderr,"amax=%13.5e\n",amax);
	 */
	//close(fdw);


	MPI_Type_free(&tsheader_mpi);
	MPI_Type_free(&tsheader_procP3_mpi);
	MPI_Finalize();

	return 0;
}

long long_swap(char *cbuf)
{
	union
	{
		char cval[4];
		long lval;
	} l_union;

	l_union.cval[3] = cbuf[0];
	l_union.cval[2] = cbuf[1];
	l_union.cval[1] = cbuf[2];
	l_union.cval[0] = cbuf[3];

	return(l_union.lval);
}

float float_swap(char *cbuf)
{
	union
	{
		char cval[4];
		float fval;
	} f_union;

	f_union.cval[3] = cbuf[0];
	f_union.cval[2] = cbuf[1];
	f_union.cval[1] = cbuf[2];
	f_union.cval[0] = cbuf[3];

	return(f_union.fval);
}

void swap_in_place(int n,char *cbuf)
{
	char cv;

	while(n--)
	{
		cv = cbuf[0];
		cbuf[0] = cbuf[3];
		cbuf[3] = cv;

		cv = cbuf[1];
		cbuf[1] = cbuf[2];
		cbuf[2] = cv;

		cbuf = cbuf + 4;
	}
}
