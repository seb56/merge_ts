#include        "include.h"
#include        "structure.h"
#include        "function.h"
#include <assert.h>

#include <sys/stat.h>
#include <unistd.h>

float float_swap(char *);

#define JJ 3

main(int argc, char **argv)
{
	FILE *fpr, *fopfile();
	struct tsheader tshead1, tshead2;
	struct tsheader_procP3 *tshead_pP3;

	struct stat st1, st2;


	int i, j, iy, it, ip;
	int nfiles;
	float *v1, *v2;

	int k;
	float max, amax;

	int min_ix0, min_iy0, min_iz0, min_it0;
	off_t size1, size2;

	int width;


	int swap_bytes = 0;
	char cbuf[512];

	off_t off, cur_off, head_off, nrite, nxblen;

	int fdr1, fdr2, fdw;
	char str[512];
	char *filebuf;
	char infile1[256], infile2[256];
	char filelist[256];

	char outfile[256];
	char tmp[128];

	char cwd[128];
	clock_t begin, end;
	double time_spent;


	if (argc<3) {
		fprintf(stderr,"Usage:%s file1 file2\n",argv[0]);
		exit(0);
	}

	strcpy(infile1,argv[1]);
	strcpy(infile2,argv[2]);

	fprintf(stderr,"Inputfile:\n%s\n%s\n",infile1, infile2);


	begin = clock();




	strcpy(tmp, cwd);
	strcat(tmp,"/");
	strcat(tmp,infile1);
	strcpy(infile1,tmp);

	fdr1 = opfile_ro(infile1);
	//stat(infile1,&st1); //option 1
	//size1=st1.st_size;

	size1=lseek(fdr1,0,SEEK_END); //option 2
	lseek(fdr1,0,SEEK_SET);

//	fseeko(fdr1, 0L, SEEK_END); //option 3...doesn't work for large files...
//	size1 = ftello(fdr1);
//	fseeko(fdr1, 0, SEEK_SET); //fseek and fseeko don't work for large files...




	strcpy(tmp, cwd);
	strcat(tmp,"/");
	strcat(tmp,infile2);
	strcpy(infile2,tmp);

	fdr2 = opfile_ro(infile2);
	size2=lseek(fdr2,0,SEEK_END);
	lseek(fdr2,0,SEEK_SET);


	fprintf(stderr,"size1=%ld size2=%ld\n",size1,size2);
	assert(size1==size2);



	reed(fdr1,&tshead1,sizeof(struct tsheader));
	reed(fdr2,&tshead2,sizeof(struct tsheader));


	//for(it=0;it<tshead.nt;it++)
	//	rite(fdw,v1,3*tshead.nx*tshead.ny*sizeof(float));

	fprintf(stderr,"ix0= %d\n",tshead1.ix0);
	fprintf(stderr,"iy0= %d\n",tshead1.iy0);
	fprintf(stderr,"iz0= %d\n",tshead1.iz0);
	fprintf(stderr,"it0= %d\n",tshead1.it0);
	fprintf(stderr,"nx= %d dx= %lg\n",tshead1.nx,tshead1.dx);
	fprintf(stderr,"ny= %d dy= %lg\n",tshead1.ny,tshead1.dy);
	fprintf(stderr,"nz= %d dz= %lg\n",tshead1.nz,tshead1.dz);
	fprintf(stderr,"nt= %d dt= %lg\n",tshead1.nt,tshead1.dt);
	fprintf(stderr,"modelrot= %lf modellat= %lg modellon=%lg \n",tshead1.modelrot,tshead1.modellat, tshead1.modellon);
	fflush(stderr);


	fprintf(stderr,"ix0= %d\n",tshead2.ix0);
	fprintf(stderr,"iy0= %d\n",tshead2.iy0);
	fprintf(stderr,"iz0= %d\n",tshead2.iz0);
	fprintf(stderr,"it0= %d\n",tshead2.it0);
	fprintf(stderr,"nx= %d dx= %lg\n",tshead2.nx,tshead2.dx);
	fprintf(stderr,"ny= %d dy= %lg\n",tshead2.ny,tshead2.dy);
	fprintf(stderr,"nz= %d dz= %lg\n",tshead2.nz,tshead2.dz);
	fprintf(stderr,"nt= %d dt= %lg\n",tshead2.nt,tshead2.dt);
	fprintf(stderr,"modelrot= %lf modellat= %lg modellon=%lg \n",tshead2.modelrot,tshead2.modellat, tshead2.modellon);
	fflush(stderr);


	assert(tshead1.ix0==tshead2.ix0);
	assert(tshead1.iy0==tshead2.iy0);
	assert(tshead1.iz0==tshead2.iz0);
	assert(tshead1.it0==tshead2.it0);
	assert(tshead1.nx==tshead2.nx);
	assert(tshead1.ny==tshead2.ny);
	assert(tshead1.nz==tshead2.nz);
	assert(tshead1.nt==tshead2.nt);
	assert(tshead1.dx==tshead2.dx);
	assert(tshead1.dy==tshead2.dy);
	assert(tshead1.dz==tshead2.dz);
	assert(tshead1.dt==tshead2.dt);
	assert(tshead1.modelrot==tshead2.modelrot);
	assert(tshead1.modellat==tshead2.modellat);
	assert(tshead1.modellon==tshead2.modellon);


	width = tshead1.nx*tshead1.ny;


	v1 = (float *) check_malloc (JJ*width*sizeof(float)); //read buffer owned/used by each process. Size is allocated very generously here.
	v2 = (float *) check_malloc (JJ*width*sizeof(float)); //read buffer owned/used by each process. Size is allocated very generously here.

	int error=0;
	for(it=0;it<tshead1.nt;it++) {
		int ierror=0;
		fprintf(stderr,"it=%d\n",it);

		reed(fdr1,v1,JJ*width*sizeof(float));
		reed(fdr2,v2,JJ*width*sizeof(float));
		for (j=0;j<JJ;j++) {
			for (i=0;i<width;i++) {
				if(v1[j*width+i]!=v2[j*width+i]) {
					fprintf(stderr,"ERROR: it=%d j=%d position:%d value1=%f value2=%f\n",it,j,j*width+i,v1[j*width+i],v2[j*width+i]);
					ierror++;
				}
			}
		}
		if (ierror>0) {
			fprintf(stderr,"it=%d %d errors\n",it,error);
			for (i=0;i<JJ*width;i++) {
				fprintf(stderr,"%f ",v1[i]);
			}
			fprintf(stderr,"\n\n");

			for (i=0;i<JJ*width;i++) {
				fprintf(stderr,"%f ",v2[i]);
			}
			fprintf(stderr,"\n\n");

		}
		error+=ierror;
	}

	fprintf(stderr,"Total %d errors\n",error);

//		fprintf(stderr,"it=%d(/%d)\n",it,tshead.nt);
//		for (i=0;i<3*tshead.nx*tshead.ny;i++) {
//			fprintf(stderr,"%f ",v1[i]);
//		}
//		fprintf(stderr,"\n");
//	}
/*

	lseek(fdw,sizeof(struct tsheader),SEEK_SET);
	head_off = sizeof(struct tsheader);
	cur_off = sizeof(struct tsheader);

	nxblen = (off_t)(tshead.nx)*sizeof(float);

	amax = -1;




	for(i=0;i<nfiles;i++)
	{
		fprintf(stderr,"%d of %d\n",i+1,nfiles);
		fflush(stderr);

		max = 0.0;

		if(tshead_pP3[i].loc_nx > 0 && tshead_pP3[i].loc_ny > 0 && tshead_pP3[i].loc_nz > 0)
		{
			fdr = opfile_ro(infile[i]);
			lseek(fdr,sizeof(struct tsheader_procP3),SEEK_SET); //for each input file, skip the header part.

			for(it=0;it<tshead.nt;it++) //for each time slice
			{
				for(j=0;j<3;j++) //do this three times (why?)
				{
					reed(fdr,v1,tshead_pP3[i].loc_nx*tshead_pP3[i].loc_ny*sizeof(float));//read from infile[i] all local points on x-y grid and keep at v1. sized loc_nx*loc_ny
					//fprintf(stderr,"i=%d it=%d j=%d v1[0..5]={%f,%f,%f,%f,%f,}\n",i,it,j,v1[0],v1[1],v1[2],v1[3],v1[4]);

					if(swap_bytes)
						swap_in_place(tshead_pP3[i].loc_nx*tshead_pP3[i].loc_ny,(char *)(v1));



					for(iy=0;iy<tshead_pP3[i].loc_ny;iy++) //along y axis
					{
						ip = iy*tshead_pP3[i].loc_nx; //number of points written so far (from v1) v1's contents size is loc_nx*loc_ny

//						//original
//						off = head_off
//								+ (off_t)((3*it+j)*tshead.nx*tshead.ny)*sizeof(float) //output file has 3 blocks of size nx*ny per each timeslice (j=0..2). skip all past timeslices and blocks
//								+ (off_t)((iy+tshead_pP3[i].iy0)*tshead.nx)*sizeof(float) //inside nx*ny block, skip nx*iy0 (global index) and nx*iy (local index) of first point in y-axis
//								+ (off_t)(tshead_pP3[i].ix0)*sizeof(float)//then skip ix0 (global index) of first point in x-axis
//								- cur_off;
//
//						lseek(fdw,off,SEEK_CUR);
//
//						nrite = rite(fdw,&v1[ip],tshead_pP3[i].loc_nx*sizeof(float));
//						cur_off = nrite + off + cur_off;



						//sung
						off = head_off
								+ (off_t)((3*it+j)*tshead.nx*tshead.ny)*sizeof(float) //output file has 3 blocks of size nx*ny per each timeslice (j=0..2). skip all past timeslices and blocks
								+ (off_t)((iy+tshead_pP3[i].iy0)*tshead.nx)*sizeof(float) //inside nx*ny block, skip nx*iy0 (global index) and nx*iy (local index) of first point in y-axis
								+ (off_t)(tshead_pP3[i].ix0)*sizeof(float);//then skip ix0 (global index) of first point in x-axis

						lseek(fdw,off,SEEK_SET);

						//printf("i=%d it=%d j=%d iy=%d off=%d width=%d\n",i,it,j,iy,off,tshead_pP3[i].loc_nx);
						nrite = rite(fdw,&v1[ip],tshead_pP3[i].loc_nx*sizeof(float));


						//cur_off = nrite + off; //cur_off not needed


					}
				}
			}

			close(fdr);
		}


	*/
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	fprintf(stderr,"Writing files took %f seconds\n",time_spent);
	close(fdr1);
	close(fdr2);
	return 0;

}

long long_swap(char *cbuf)
{
	union
	{
		char cval[4];
		long lval;
	} l_union;

	l_union.cval[3] = cbuf[0];
	l_union.cval[2] = cbuf[1];
	l_union.cval[1] = cbuf[2];
	l_union.cval[0] = cbuf[3];

	return(l_union.lval);
}

float float_swap(char *cbuf)
{
	union
	{
		char cval[4];
		float fval;
	} f_union;

	f_union.cval[3] = cbuf[0];
	f_union.cval[2] = cbuf[1];
	f_union.cval[1] = cbuf[2];
	f_union.cval[0] = cbuf[3];

	return(f_union.fval);
}

void swap_in_place(int n,char *cbuf)
{
	char cv;

	while(n--)
	{
		cv = cbuf[0];
		cbuf[0] = cbuf[3];
		cbuf[3] = cv;

		cv = cbuf[1];
		cbuf[1] = cbuf[2];
		cbuf[2] = cv;

		cbuf = cbuf + 4;
	}
}
