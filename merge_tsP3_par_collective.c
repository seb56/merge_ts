#include        "include.h"
#include        "structure.h"
#include        "function.h"
#include <unistd.h>
#include "mpi.h"

float float_swap(char *);
#define JJ 3

main(int ac, char **av)
{

	FILE *fpr, *fopfile();
	struct tsheader tshead;
	struct tsheader_procP3 *tshead_pP3;
	int i, j, iy, it, ip;
	int nfiles;
	float *v1;

	int k;
	float max, amax;

	int min_ix0, min_iy0, min_iz0, min_it0;
	int max_loc_nx, max_loc_ny;

	int swap_bytes = 0;
	char cbuf[512];

	off_t off, cur_off, head_off, nrite, nxblen;

	int fdr, fdw;
	char str[512];
	char *filebuf;
	char **infile;
	char *ifile;

	char filelist[256];
	char outfile[256];
	char tmp[128];
	char cwd[128];


	int rank,size, num_groups, group_size, group_id, group_rank;
	MPI_File fhw;
	MPI_Status status;
	MPI_Offset offset, disp;
	MPI_File fdw_mpi, fdr_mpi;

	double starttime,endtime;


	MPI_Init(&ac, &av);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	group_size = JJ;
	num_groups = size / group_size;
	group_id = rank / group_size ;
	group_rank = rank % group_size;

	// if size=6, numgroups=2 . rank 0...2 will be on group_id 0, rank 3..5 will be on group_id 1.


	if (rank ==0) fprintf(stderr,"size=%d\n",size);

	if(getcwd(cwd,sizeof(cwd)) != NULL) {
		fprintf(stdout, "Current working dir:%s\n",cwd);
	}
	else {
		fprintf(stderr,"getcwd() error");
		exit(-1);
	}

	setpar(ac, av);
	mstpar("filelist","s",filelist);
	mstpar("outfile","s",outfile);
	mstpar("nfiles","d",&nfiles);
	getpar("swap_bytes","d",&swap_bytes);
	endpar();

	tshead_pP3 = (struct tsheader_procP3 *) check_malloc (nfiles*sizeof(struct tsheader_procP3)); //space for keeping header of all files (each processor timeslice headers)
	filebuf = (char *) check_malloc (256*nfiles*sizeof(char));
	infile = (char **) check_malloc (nfiles*sizeof(char *));


	strcpy(tmp, cwd);
	strcat(tmp,"/");
	strcat(tmp,filelist);
	strcpy(filelist,tmp);

	fpr = fopfile(filelist,"r");

	min_ix0 = 99999999;
	min_iy0 = 99999999;
	min_iz0 = 99999999;
	min_it0 = 99999999;

	max_loc_nx = max_loc_ny= -99999999;


	//tsheader struct
	int tsheader_blocklengths[15]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
	MPI_Datatype tsheader_types[15]={MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT};
	MPI_Datatype tsheader_mpi;
	MPI_Datatype onewriteblock, bulk_write_view, onereadblock, bulk_read_view,alltime_bulk_write_view;
	int *blklens, *displacements;

	MPI_Aint tsheader_disp[15];
	MPI_Get_address(&tshead.ix0, &tsheader_disp[0]);
	MPI_Get_address(&tshead.iy0, &tsheader_disp[1]);
	MPI_Get_address(&tshead.iz0, &tsheader_disp[2]);
	MPI_Get_address(&tshead.it0, &tsheader_disp[3]);
	MPI_Get_address(&tshead.nx, &tsheader_disp[4]);
	MPI_Get_address(&tshead.ny, &tsheader_disp[5]);
	MPI_Get_address(&tshead.nz, &tsheader_disp[6]);
	MPI_Get_address(&tshead.nt, &tsheader_disp[7]);
	MPI_Get_address(&tshead.dx, &tsheader_disp[8]);
	MPI_Get_address(&tshead.dy, &tsheader_disp[9]);
	MPI_Get_address(&tshead.dz, &tsheader_disp[10]);
	MPI_Get_address(&tshead.dt, &tsheader_disp[11]);
	MPI_Get_address(&tshead.modelrot, &tsheader_disp[12]);
	MPI_Get_address(&tshead.modellat, &tsheader_disp[13]);
	MPI_Get_address(&tshead.modellon, &tsheader_disp[14]);


	MPI_Aint base;
	base = tsheader_disp[0];
	for (i=0;i<15;i++) tsheader_disp[i] -= base;
	MPI_Type_create_struct(15,tsheader_blocklengths,tsheader_disp,tsheader_types, &tsheader_mpi);
	MPI_Type_commit(&tsheader_mpi);


	//tsheader_procP3 struct
	int tsheader_procP3_blocklengths[18]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
	MPI_Datatype tsheader_procP3_types[18]={MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT};
	MPI_Datatype tsheader_procP3_mpi;

	MPI_Aint tsheader_procP3_disp[18];
	MPI_Get_address(&tshead_pP3[0].ix0, &tsheader_procP3_disp[0]);
	MPI_Get_address(&tshead_pP3[0].iy0, &tsheader_procP3_disp[1]);
	MPI_Get_address(&tshead_pP3[0].iz0, &tsheader_procP3_disp[2]);
	MPI_Get_address(&tshead_pP3[0].it0, &tsheader_procP3_disp[3]);
	MPI_Get_address(&tshead_pP3[0].loc_nx, &tsheader_procP3_disp[4]);
	MPI_Get_address(&tshead_pP3[0].loc_ny, &tsheader_procP3_disp[5]);
	MPI_Get_address(&tshead_pP3[0].loc_nz, &tsheader_procP3_disp[6]);
	MPI_Get_address(&tshead_pP3[0].nx, &tsheader_procP3_disp[7]);
	MPI_Get_address(&tshead_pP3[0].ny, &tsheader_procP3_disp[8]);
	MPI_Get_address(&tshead_pP3[0].nz, &tsheader_procP3_disp[9]);
	MPI_Get_address(&tshead_pP3[0].nt, &tsheader_procP3_disp[10]);
	MPI_Get_address(&tshead_pP3[0].dx, &tsheader_procP3_disp[11]);
	MPI_Get_address(&tshead_pP3[0].dy, &tsheader_procP3_disp[12]);
	MPI_Get_address(&tshead_pP3[0].dz, &tsheader_procP3_disp[13]);
	MPI_Get_address(&tshead_pP3[0].dt, &tsheader_procP3_disp[14]);
	MPI_Get_address(&tshead_pP3[0].modelrot, &tsheader_procP3_disp[15]);
	MPI_Get_address(&tshead_pP3[0].modellat, &tsheader_procP3_disp[16]);
	MPI_Get_address(&tshead_pP3[0].modellon, &tsheader_procP3_disp[17]);

	//MPI_Aint base;
	base = tsheader_procP3_disp[0];
	for (i=0;i<18;i++) tsheader_procP3_disp[i] -= base;

	MPI_Type_create_struct(18,tsheader_procP3_blocklengths,tsheader_procP3_disp,tsheader_procP3_types, &tsheader_procP3_mpi);
	MPI_Type_commit(&tsheader_procP3_mpi);



	if (rank==0)
	{
		/* the following read part can be done in parallel too*/

		i = 0;
		while(fscanf(fpr,"%s",str) != EOF && i < nfiles)
		{
			infile[i] = filebuf + i*256;//pointing to 256-width slice of filebuf
			strcpy(infile[i],str);

			fprintf(stderr,"%d %s\n",i,infile[i]);

			strcpy(tmp, cwd);
			strcat(tmp,"/");
			strcat(tmp,infile[i]);
			strcpy(infile[i],tmp);

			fdr = opfile_ro(infile[i]);
			reed(fdr,&tshead_pP3[i],sizeof(struct tsheader_procP3));
			close(fdr);

			if(swap_bytes)
			{
				swap_in_place(1,(char *)(&tshead_pP3[i].ix0));
				swap_in_place(1,(char *)(&tshead_pP3[i].iy0));
				swap_in_place(1,(char *)(&tshead_pP3[i].iz0));
				swap_in_place(1,(char *)(&tshead_pP3[i].it0));
				swap_in_place(1,(char *)(&tshead_pP3[i].loc_nx));
				swap_in_place(1,(char *)(&tshead_pP3[i].loc_ny));
				swap_in_place(1,(char *)(&tshead_pP3[i].loc_nz));
				swap_in_place(1,(char *)(&tshead_pP3[i].nx));
				swap_in_place(1,(char *)(&tshead_pP3[i].ny));
				swap_in_place(1,(char *)(&tshead_pP3[i].nz));
				swap_in_place(1,(char *)(&tshead_pP3[i].nt));
				swap_in_place(1,(char *)(&tshead_pP3[i].dx));
				swap_in_place(1,(char *)(&tshead_pP3[i].dy));
				swap_in_place(1,(char *)(&tshead_pP3[i].dz));
				swap_in_place(1,(char *)(&tshead_pP3[i].dt));
				swap_in_place(1,(char *)(&tshead_pP3[i].modelrot));
				swap_in_place(1,(char *)(&tshead_pP3[i].modellat));
				swap_in_place(1,(char *)(&tshead_pP3[i].modellon));
			}

			if(tshead_pP3[i].ix0 < min_ix0)
				min_ix0 = tshead_pP3[i].ix0;
			if(tshead_pP3[i].iy0 < min_iy0)
				min_iy0 = tshead_pP3[i].iy0;
			if(tshead_pP3[i].iz0 < min_iz0)
				min_iz0 = tshead_pP3[i].iz0;
			if(tshead_pP3[i].it0 < min_it0)
				min_it0 = tshead_pP3[i].it0;
			i++;
		}

		if(i != nfiles)
		{
			fprintf(stderr,"(%d) entries in filelist != nfiles(%d), exiting ...\n",i,nfiles);
			exit(-1);
		}

		tshead.ix0 = min_ix0;
		tshead.iy0 = min_iy0;
		tshead.iz0 = min_iz0;
		tshead.it0 = min_it0;
		tshead.nx = tshead_pP3[0].nx;
		tshead.ny = tshead_pP3[0].ny;
		tshead.nz = tshead_pP3[0].nz;
		tshead.nt = tshead_pP3[0].nt;
		tshead.dx = tshead_pP3[0].dx;
		tshead.dy = tshead_pP3[0].dy;
		tshead.dz = tshead_pP3[0].dz;
		tshead.dt = tshead_pP3[0].dt;
		tshead.modelrot = tshead_pP3[0].modelrot;
		tshead.modellat = tshead_pP3[0].modellat;
		tshead.modellon = tshead_pP3[0].modellon;

		fprintf(stderr,"ix0= %d\n",tshead.ix0);
		fprintf(stderr,"iy0= %d\n",tshead.iy0);
		fprintf(stderr,"nx= %d dx= %lg\n",tshead.nx,tshead.dx);
		fprintf(stderr,"ny= %d dy= %lg\n",tshead.ny,tshead.dy);
		fprintf(stderr,"nt= %d dt= %lg\n",tshead.nt,tshead.dt);
		fflush(stderr);

//		MPI_Send(&tshead, 1, tsheader_mpi,1,0,MPI_COMM_WORLD);

	}

	MPI_Bcast(&tshead, 1, tsheader_mpi,0,MPI_COMM_WORLD);
	MPI_Bcast(&tshead_pP3[0], nfiles, tsheader_procP3_mpi,0,MPI_COMM_WORLD);
	MPI_Bcast(&filebuf[0], 256*nfiles, MPI_CHAR, 0, MPI_COMM_WORLD);


	if (rank!=0) {
		//MPI_Recv(&tshead, 1, tsheader_mpi,0,0, MPI_COMM_WORLD,&status);
		fprintf(stderr,"!! ix0= %d\n",tshead.ix0);
		fprintf(stderr,"!! iy0= %d\n",tshead.iy0);
		fprintf(stderr,"!! nx= %d dx= %lg\n",tshead.nx,tshead.dx);
		fprintf(stderr,"!! ny= %d dy= %lg\n",tshead.ny,tshead.dy);
		fprintf(stderr,"!!nt= %d dt= %lg\n",tshead.nt,tshead.dt);

	}


	v1 = (float *) check_malloc (tshead.nt*tshead.nx*tshead.ny*sizeof(float));
	for(iy=0;iy<tshead.nt*tshead.nx*tshead.ny;iy++)
		v1[iy] = 0;


	strcpy(tmp, cwd);
	strcat(tmp,"/");
	strcat(tmp,outfile);
	strcpy(outfile,tmp);


	starttime=MPI_Wtime();

	if (rank==0) {

		fdw = croptrfile(outfile);
		rite(fdw,&tshead,sizeof(struct tsheader));;


		for(it=0;it<tshead.nt;it++) {
			//could write with tshead.nx*tshead.ny*3 without the inner for loop. BUT if tshead.nt < 3, v1 is too short.
			for (j=0;j<JJ;j++) rite(fdw,&v1[0],tshead.nx*tshead.ny*sizeof(float));
		}


		lseek(fdw,sizeof(struct tsheader),SEEK_SET);

		close(fdw);
	}


	endtime = MPI_Wtime();
	if (rank ==0) fprintf(stderr,"Creating zero-filled file took %f seconds\n",endtime-starttime);


	head_off = sizeof(struct tsheader);
	cur_off = sizeof(struct tsheader);

	nxblen = (off_t)(tshead.nx)*sizeof(float);

	amax = -1;

	blklens = (int*)malloc(sizeof(int)*tshead.ny*tshead.nt);
	displacements = (int*)malloc(sizeof(int)*tshead.ny*tshead.nt);


	starttime=MPI_Wtime();
	MPI_File_open(MPI_COMM_WORLD,outfile, MPI_MODE_CREATE|MPI_MODE_RDWR,MPI_INFO_NULL,&fdw_mpi);

	//for(i=nfiles/size*rank;i<nfiles/size*(rank+1);i++)
	//for(i=0;i<nfiles;i++)
	for(i=nfiles/num_groups*group_id;i<nfiles/num_groups*(group_id+1);i++)
	{
		fprintf(stderr,"rank=%d num_groups=%d for (%d~%d) group_id=%d group_rank=%d (%d of %d)\n",rank, num_groups, nfiles/num_groups*group_id, nfiles/num_groups*(group_id+1),group_id, group_rank, i+1,nfiles);
		fflush(stderr);

		max = 0.0;
		int read_width = tshead_pP3[i].loc_nx*tshead_pP3[i].loc_ny;

		MPI_Type_vector(tshead.nt, read_width, read_width*JJ, MPI_FLOAT, &bulk_read_view);
		MPI_Type_commit(&bulk_read_view);

		MPI_Type_contiguous(read_width, MPI_FLOAT, &onereadblock);
		MPI_Type_commit(&onereadblock);

		k=0;
		int block_begin;
		for (it=0;it<tshead.nt;it++) { //within the same timeslice block
			block_begin = JJ*tshead.nx*tshead.ny*it; //timeslice block starts with this displacement.
			for(iy=0;iy<tshead_pP3[i].loc_ny;iy++) {
				blklens[k]= tshead_pP3[i].loc_nx;
				displacements[k]=block_begin+tshead.nx*iy; //each write has a stride of "tshead.nx" within the same timeslice block.
				k++;
			}
		}
		MPI_Type_indexed(tshead_pP3[i].loc_ny*tshead.nt,blklens,displacements,MPI_FLOAT, &alltime_bulk_write_view);
		MPI_Type_commit(&alltime_bulk_write_view);


		j=group_rank;

		if(tshead_pP3[i].loc_nx > 0 && tshead_pP3[i].loc_ny > 0 && tshead_pP3[i].loc_nz > 0)
		{
			ifile = filebuf+i*256;


			MPI_File_open(MPI_COMM_WORLD, ifile, MPI_MODE_RDONLY, MPI_INFO_NULL, &fdr_mpi);

			//JJ procs read in parallel, loc_nx * loc_ny width each., with disp=rank*loc_nx*loc_ny*it

			MPI_File_set_view(fdr_mpi,sizeof(struct tsheader_procP3)+read_width*j*sizeof(float), MPI_FLOAT, bulk_read_view, "native",MPI_INFO_NULL);
			MPI_File_read_all(fdr_mpi,v1,tshead.nt,onereadblock,&status);

			if(swap_bytes) swap_in_place(read_width*tshead.nt,(char *)(v1)); //may not work. v1's index needs to be adjusted
/*
			for(it=0;it<tshead.nt;it++) //for each time slice
			{
   			fprintf(stderr,"i=%d it=%d j=%d v1[0..5]={%f,%f,%f,%f,%f,}\n",i,it,j,v1[it*read_width],v1[it*read_width+1],v1[it*read_width+2],v1[it*read_width+JJ],v1[it*read_width+4]);

			}
*/

			disp = head_off //size of header to skip
					+ (off_t)(j*tshead.nx*tshead.ny)*sizeof(float) //output file has JJ blocks of size nx*ny per each timeslice (j=0..2). skip all past timeslices and blocks
					+ (off_t)((tshead_pP3[i].iy0)*tshead.nx)*sizeof(float) //inside nx*ny block, skip nx*iy0 (global index) and nx*iy (local index) of first point in y-axis
					+ (off_t)(tshead_pP3[i].ix0)*sizeof(float);//then skip ix0 (global index) of first point in x-axis

			MPI_File_set_view(fdw_mpi, disp, MPI_FLOAT, alltime_bulk_write_view, "native", MPI_INFO_NULL);
			//MPI_File_write_all(fdw_mpi, (void *)(&v1[0]),tshead.nt, bulk_write_view,&status);
			MPI_File_write_all(fdw_mpi, (void *)(&v1[0]),tshead.nt*tshead_pP3[i].loc_nx*tshead_pP3[i].loc_ny, MPI_FLOAT,&status);

			MPI_File_close(&fdr_mpi);
		}
		MPI_Type_free(&alltime_bulk_write_view);
		//MPI_Type_free(&bulk_write_view);

		MPI_Type_free(&onereadblock);
		MPI_Type_free(&bulk_read_view);

		/*
fprintf(stderr,"max=%13.5e\n",max);
if(max>amax) amax = max;
		 */

	}
	MPI_File_close(&fdw_mpi);
	endtime = MPI_Wtime();
	MPI_Barrier(MPI_COMM_WORLD);
	if (rank ==0) fprintf(stderr,"Writing files took %f seconds\n",endtime-starttime);
	/*
fprintf(stderr,"amax=%13.5e\n",amax);
	 */
	//close(fdw);



	MPI_Type_free(&tsheader_mpi);
	MPI_Finalize();
}

long long_swap(char *cbuf)
{
	union
	{
		char cval[4];
		long lval;
	} l_union;

	l_union.cval[3] = cbuf[0];
	l_union.cval[2] = cbuf[1];
	l_union.cval[1] = cbuf[2];
	l_union.cval[0] = cbuf[3];

	return(l_union.lval);
}

float float_swap(char *cbuf)
{
	union
	{
		char cval[4];
		float fval;
	} f_union;

	f_union.cval[3] = cbuf[0];
	f_union.cval[2] = cbuf[1];
	f_union.cval[1] = cbuf[2];
	f_union.cval[0] = cbuf[3];

	return(f_union.fval);
}

void swap_in_place(int n,char *cbuf)
{
	char cv;

	while(n--)
	{
		cv = cbuf[0];
		cbuf[0] = cbuf[3];
		cbuf[3] = cv;

		cv = cbuf[1];
		cbuf[1] = cbuf[2];
		cbuf[2] = cv;

		cbuf = cbuf + 4;
	}
}
