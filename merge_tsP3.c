#include        "include.h"
#include        "structure.h"
#include        "function.h"
#include <unistd.h>

float float_swap(char *);

main(int ac, char **av)
{
	FILE *fpr, *fopfile();
	struct tsheader tshead;
	struct tsheader_procP3 *tshead_pP3;
	int i, j, iy, it, ip;
	int nfiles;
	float *v1;

	int k;
	float max, amax;

	int min_ix0, min_iy0, min_iz0, min_it0;

	int swap_bytes = 0;
	char cbuf[512];

	off_t off, cur_off, head_off, nrite, nxblen;

	int fdr, fdw;
	char str[512];
	char *filebuf;
	char **infile;
	char filelist[256];
	char outfile[256];
	char tmp[128];

	char cwd[128];
	clock_t begin, end;
	double time_spent;



	if(getcwd(cwd,sizeof(cwd)) != NULL) {
		fprintf(stdout, "Current working dir:%s\n",cwd);
	}
	else {
		fprintf(stderr,"getcwd() error");
		exit(-1);
	}

	begin = clock();

	setpar(ac, av);
	mstpar("filelist","s",filelist);
	mstpar("outfile","s",outfile);
	mstpar("nfiles","d",&nfiles);
	getpar("swap_bytes","d",&swap_bytes);
	endpar();

	tshead_pP3 = (struct tsheader_procP3 *) check_malloc (nfiles*sizeof(struct tsheader_procP3)); //space for keeping header of all files (each processor timeslice headers)
	filebuf = (char *) check_malloc (256*nfiles*sizeof(char));
	infile = (char **) check_malloc (nfiles*sizeof(char *));


	strcpy(tmp, cwd);
	strcat(tmp,"/");
	strcat(tmp,filelist);
	strcpy(filelist,tmp);

	fpr = fopfile(filelist,"r");

	min_ix0 = 99999999;
	min_iy0 = 99999999;
	min_iz0 = 99999999;
	min_it0 = 99999999;
	i = 0;
	while(fscanf(fpr,"%s",str) != EOF && i < nfiles)
	{
		infile[i] = filebuf + i*256;
		strcpy(infile[i],str);

		fprintf(stderr,"%d %s\n",i,infile[i]);

		strcpy(tmp, cwd);
		strcat(tmp,"/");
		strcat(tmp,infile[i]);
		strcpy(infile[i],tmp);

		fdr = opfile_ro(infile[i]);
		reed(fdr,&tshead_pP3[i],sizeof(struct tsheader_procP3));
		close(fdr);

		if(swap_bytes)
		{
			swap_in_place(1,(char *)(&tshead_pP3[i].ix0));
			swap_in_place(1,(char *)(&tshead_pP3[i].iy0));
			swap_in_place(1,(char *)(&tshead_pP3[i].iz0));
			swap_in_place(1,(char *)(&tshead_pP3[i].it0));
			swap_in_place(1,(char *)(&tshead_pP3[i].loc_nx));
			swap_in_place(1,(char *)(&tshead_pP3[i].loc_ny));
			swap_in_place(1,(char *)(&tshead_pP3[i].loc_nz));
			swap_in_place(1,(char *)(&tshead_pP3[i].nx));
			swap_in_place(1,(char *)(&tshead_pP3[i].ny));
			swap_in_place(1,(char *)(&tshead_pP3[i].nz));
			swap_in_place(1,(char *)(&tshead_pP3[i].nt));
			swap_in_place(1,(char *)(&tshead_pP3[i].dx));
			swap_in_place(1,(char *)(&tshead_pP3[i].dy));
			swap_in_place(1,(char *)(&tshead_pP3[i].dz));
			swap_in_place(1,(char *)(&tshead_pP3[i].dt));
			swap_in_place(1,(char *)(&tshead_pP3[i].modelrot));
			swap_in_place(1,(char *)(&tshead_pP3[i].modellat));
			swap_in_place(1,(char *)(&tshead_pP3[i].modellon));
		}

		if(tshead_pP3[i].ix0 < min_ix0)
			min_ix0 = tshead_pP3[i].ix0;
		if(tshead_pP3[i].iy0 < min_iy0)
			min_iy0 = tshead_pP3[i].iy0;
		if(tshead_pP3[i].iz0 < min_iz0)
			min_iz0 = tshead_pP3[i].iz0;
		if(tshead_pP3[i].it0 < min_it0)
			min_it0 = tshead_pP3[i].it0;

		i++;
	}

	if(i != nfiles)
	{
		fprintf(stderr,"(%d) entries in filelist != nfiles(%d), exiting ...\n",i,nfiles);
		exit(-1);
	}

	tshead.ix0 = min_ix0;
	tshead.iy0 = min_iy0;
	tshead.iz0 = min_iz0;
	tshead.it0 = min_it0;
	tshead.nx = tshead_pP3[0].nx;
	tshead.ny = tshead_pP3[0].ny;
	tshead.nz = tshead_pP3[0].nz;
	tshead.nt = tshead_pP3[0].nt;
	tshead.dx = tshead_pP3[0].dx;
	tshead.dy = tshead_pP3[0].dy;
	tshead.dz = tshead_pP3[0].dz;
	tshead.dt = tshead_pP3[0].dt;
	tshead.modelrot = tshead_pP3[0].modelrot;
	tshead.modellat = tshead_pP3[0].modellat;
	tshead.modellon = tshead_pP3[0].modellon;

	fprintf(stderr,"ix0= %d\n",tshead.ix0);
	fprintf(stderr,"iy0= %d\n",tshead.iy0);
	fprintf(stderr,"nx= %d dx= %lg\n",tshead.nx,tshead.dx);
	fprintf(stderr,"ny= %d dy= %lg\n",tshead.ny,tshead.dy);
	fprintf(stderr,"nt= %d dt= %lg\n",tshead.nt,tshead.dt);
	fflush(stderr);

	v1 = (float *) check_malloc (3*tshead.nx*tshead.ny*sizeof(float));
	for(iy=0;iy<3*tshead.nx*tshead.ny;iy++)
		v1[iy] = 0;

	strcpy(tmp, cwd);
	strcat(tmp,"/");
	strcat(tmp,outfile);
	strcpy(outfile,tmp);

	fdw = croptrfile(outfile);
	rite(fdw,&tshead,sizeof(struct tsheader));;

	for(it=0;it<tshead.nt;it++)
		rite(fdw,v1,3*tshead.nx*tshead.ny*sizeof(float));

	lseek(fdw,sizeof(struct tsheader),SEEK_SET);
	head_off = sizeof(struct tsheader);
	cur_off = sizeof(struct tsheader);

	nxblen = (off_t)(tshead.nx)*sizeof(float);

	amax = -1;


	/* here, do your time-consuming job */

	for(i=0;i<nfiles;i++)
	{
		fprintf(stderr,"%d of %d\n",i+1,nfiles);
		fflush(stderr);

		max = 0.0;

		if(tshead_pP3[i].loc_nx > 0 && tshead_pP3[i].loc_ny > 0 && tshead_pP3[i].loc_nz > 0)
		{
			fdr = opfile_ro(infile[i]);
			lseek(fdr,sizeof(struct tsheader_procP3),SEEK_SET); //for each input file, skip the header part.

			for(it=0;it<tshead.nt;it++) //for each time slice
			{
				for(j=0;j<3;j++) //do this three times (why?)
				{
					reed(fdr,v1,tshead_pP3[i].loc_nx*tshead_pP3[i].loc_ny*sizeof(float));//read from infile[i] all local points on x-y grid and keep at v1. sized loc_nx*loc_ny
					//fprintf(stderr,"i=%d it=%d j=%d v1[0..5]={%f,%f,%f,%f,%f,}\n",i,it,j,v1[0],v1[1],v1[2],v1[3],v1[4]);

					if(swap_bytes)
						swap_in_place(tshead_pP3[i].loc_nx*tshead_pP3[i].loc_ny,(char *)(v1));

					/*
for(k=0;k<tshead_pP3[i].loc_nx*tshead_pP3[i].loc_ny;k++)
   {
   if(v1[k]>max)
      max=v1[k];
   if(-v1[k]>max)
      max=-v1[k];
   }
					 */

					for(iy=0;iy<tshead_pP3[i].loc_ny;iy++) //along y axis
					{
						ip = iy*tshead_pP3[i].loc_nx; //number of points written so far (from v1) v1's contents size is loc_nx*loc_ny

//						//original
//						off = head_off
//								+ (off_t)((3*it+j)*tshead.nx*tshead.ny)*sizeof(float) //output file has 3 blocks of size nx*ny per each timeslice (j=0..2). skip all past timeslices and blocks
//								+ (off_t)((iy+tshead_pP3[i].iy0)*tshead.nx)*sizeof(float) //inside nx*ny block, skip nx*iy0 (global index) and nx*iy (local index) of first point in y-axis
//								+ (off_t)(tshead_pP3[i].ix0)*sizeof(float)//then skip ix0 (global index) of first point in x-axis
//								- cur_off;
//
//						lseek(fdw,off,SEEK_CUR);
//
//						nrite = rite(fdw,&v1[ip],tshead_pP3[i].loc_nx*sizeof(float));
//						cur_off = nrite + off + cur_off;



						//sung
						off = head_off
								+ (off_t)((3*it+j)*tshead.nx*tshead.ny)*sizeof(float) //output file has 3 blocks of size nx*ny per each timeslice (j=0..2). skip all past timeslices and blocks
								+ (off_t)((iy+tshead_pP3[i].iy0)*tshead.nx)*sizeof(float) //inside nx*ny block, skip nx*iy0 (global index) and nx*iy (local index) of first point in y-axis
								+ (off_t)(tshead_pP3[i].ix0)*sizeof(float);//then skip ix0 (global index) of first point in x-axis

						lseek(fdw,off,SEEK_SET);

						//printf("i=%d it=%d j=%d iy=%d off=%d width=%d\n",i,it,j,iy,off,tshead_pP3[i].loc_nx);
						nrite = rite(fdw,&v1[ip],tshead_pP3[i].loc_nx*sizeof(float));


						//cur_off = nrite + off; //cur_off not needed


					}
				}
			}

			close(fdr);
		}
		/*
fprintf(stderr,"max=%13.5e\n",max);
if(max>amax) amax = max;
		 */
	}
	/*
fprintf(stderr,"amax=%13.5e\n",amax);
	 */
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	fprintf(stderr,"Writing files took %f seconds\n",time_spent);
	close(fdw);
}

long long_swap(char *cbuf)
{
	union
	{
		char cval[4];
		long lval;
	} l_union;

	l_union.cval[3] = cbuf[0];
	l_union.cval[2] = cbuf[1];
	l_union.cval[1] = cbuf[2];
	l_union.cval[0] = cbuf[3];

	return(l_union.lval);
}

float float_swap(char *cbuf)
{
	union
	{
		char cval[4];
		float fval;
	} f_union;

	f_union.cval[3] = cbuf[0];
	f_union.cval[2] = cbuf[1];
	f_union.cval[1] = cbuf[2];
	f_union.cval[0] = cbuf[3];

	return(f_union.fval);
}

void swap_in_place(int n,char *cbuf)
{
	char cv;

	while(n--)
	{
		cv = cbuf[0];
		cbuf[0] = cbuf[3];
		cbuf[3] = cv;

		cv = cbuf[1];
		cbuf[1] = cbuf[2];
		cbuf[2] = cv;

		cbuf = cbuf + 4;
	}
}
