C RWG modified routine
C tb is adjusted such that target slip is obtained upon integration

        subroutine srctimfuncRWG(n,slip,dt,sd,aleng,rupvel,fc,shearmod,
     +                            td,tb,tr,st,npts)
        dimension st(1),disp(10000)

c  sd:     stress drop (MPa)
c  aleng : asperity characteristic length (km)
c  rupvel: rupture velocity (km/s)
c  fc    : corner frequency 
c  shearmod : shear modulus
c  vm    : slip velocity (cm/s)
c  slip
c  npts

	print *,"td,tb,tr:  ",td,tb,tr,vm
        ts=1.4*tr
        vm=sd/shearmod*sqrt(2*aleng*rupvel*fc)

CRWG	print *," sd,shearmod,aleng,rupvel,fc: ",sd,shearmod,aleng,rupvel,fc
CRWG        print*,'Max slip velocity ',vm,' m/s'

CRWG new code to END

C convert vm to units of cm/s
	vm = 100.0*vm
        kk = 1
9876    continue

CRWG END

8765    continue
        eps=(5*tb-6*td)/(1-td/tb)/2.
	if(tb.le.eps) then
	   tb = 0.99*tb
	   go to 8765
	endif

        b=2.*vm/td*tb*(tb-eps)**0.5*(1-tb/td/2)

        do 10 i=1,n
         tt=(i-1)*dt
        if(tt.le.tb) then
           st(i)=2.*vm/td*tt*(1-tt/2/td)
        else
           if(tt.le.tr) then
             st(i)=b/(tt-eps)**0.5
             c=st(i)
           else
             if(tt.le.ts) then
               st(i)=c-c/(ts-tr)*(tt-tr)
             else
               st(i)=0.
	       npts = i
	       goto 20
             endif
           endif
        endif
10      continue

20	continue

	s = 0
	do i=1,npts
	   s = s + st(i)*dt
	   disp(i) = s
	enddo
	fact = slip/disp(npts)

CRWG new code to END

	print *,"kk= ",kk," fact= ",fact," tb= ",tb," sdrop= ",sd," td= ",td," tb/td= ",tb/td

	if(fact.gt.1.01) then

	   tb = tb*0.99
c	   td = td*1.01
	   kk = kk + 1
	   go to 9876

	else if(fact.lt.0.99) then

	   tb = tb*1.01
c	   td = td*0.99
	   kk = kk + 1
	   go to 9876

	endif

CRWG END

CRWG	do i=1,npts
CRWG	   st(i) = st(i)*fact
CRWG	enddo
c	print *,"Factor= ",fact/100, " Max slip= ",disp(npts)*100
c	print *,"npts=   ",npts

        return
        end

C original subroutine from Arben

        subroutine srctimfunc(n,slip,dt,sd,aleng,rupvel,fc,shearmod,
     +                            td,tb,tr,st,npts)
C	include 'limits.h'
        dimension st(1),disp(10000)

c  sd:     stress drop (MPa)
c  aleng : asperity characteristic length (km)
c  rupvel: rupture velocity (km/s)
c  fc    : corner frequency 
c  shearmod : shear modulus
c  vm    : slip velocity (cm/s)
c  slip
c  npts

	print *,"td,tb,tr:  ",td,tb,tr
        ts=1.4*tr
        vm=sd/shearmod*sqrt(2*aleng*rupvel*fc)
	print *," sd,shearmod,aleng,rupvel,fc: ",sd,shearmod,aleng,rupvel,fc
        print*,'Max slip velocity ',vm,' m/s'
        eps=(5*tb-6*td)/(1-td/tb)/2.
        b=2.*vm/td*tb*(tb-eps)**0.5*(1-tb/td/2)

        do 10 i=1,n
         tt=(i-1)*dt
        if(tt.le.tb) then
           st(i)=2.*vm/td*tt*(1-tt/2/td)
        else
           if(tt.le.tr) then
             st(i)=b/(tt-eps)**0.5
             c=st(i)
           else
             if(tt.le.ts) then
               st(i)=c-c/(ts-tr)*(tt-tr)
             else
               st(i)=0.
	       npts = i
	       goto 20
             endif
           endif
        endif
10      continue

20	continue

	s = 0
	do i=1,npts
	   s = s + st(i)*dt
	   disp(i) = s
	enddo
	fact = slip/disp(npts)
	do i=1,npts
	   st(i) = st(i)*fact
	enddo
c	print *,"Factor= ",fact/100, " Max slip= ",disp(npts)*100
c	print *,"npts=   ",npts

        return
        end
