COBJS = sacio.o iofunc.o fft1d.o geoproj_subs.o
FOBJS = fourg.o mccamy.o zpass.o geo_utm.o miyatake.o

GETPARLIB = ./ #${HOME}/workspace/nesi00213/Getpar/getpar/lib

#GCCLIB = /usr/lib/gcc/x86_64-redhat-linux/3.4.6
#FORTLIBS = -lfrtbegin -lg2c -lgcc_s
#FC = g77

#GCCLIB = /usr/lib/gcc/x86_64-redhat-linux/4.1.1
#GCCLIB = /usr/lib64/gcc/x86_64-linux-gnu/4.8
#GCCLIB = /usr/lib64/gcc/powerpc64-suse-linux/4.3
FORTLIBS = -lgfortranbegin -lgfortran -lgcc_s
FC = gfortran

LDLIBS = ${COBJS} ${FOBJS} -L${GETPARLIB} -lm -lget ${FORTLIBS} #-L/usr/local/pkg/ipm/version/lib -Wl,-rpath=/usr/local/pkg/ipm/version/lib -lipm 

#LF_FLAGS = -D_FILE_OFFSET_BITS=32
#
# use following for large file capability
LF_FLAGS = -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64

UFLAGS = -Wall -g -pg #-O3

CC = mpicc

#BIN_LIST = ampdur_gof anal_wfilter avg_spec convcdmg decon_mi \
           fdbin2peakamp fdbin2scec fdbin2wcc finite_fault2d gen_fdcoords \
           genslip geos2wcc gf3d2asc gf3d2wcc grt2pntsrc integ_diff \
           linint_rwg linreg mech_instr mf_ampspec polyfit ratio_resp \
           read_port2wcc ref_respect reformat sac2wcc_rob scec2wcc wcc2sac \
           set_static spec_gof static_array stf_frand test_rlimit \
           usgs2wcc wcc2esg wcc2ngf wcc2scc wcc2scec wcc_2ampspec wcc_Xcor \
           wcc_add wcc_cnvlv wcc_duration wcc_durstf wcc_funcgen wcc_genstf \
           wcc_getpeak wcc_getstatic wcc_header wcc_line2pnt \
	   wcc_line2pnt_freq wcc_linresamp wcc_modfamp wcc_normamp \
	   wcc_twindow wcc_normodfilter wcc_phase+baz \
           wcc_phasevel wcc2kp wcc_phasevel_xc wcc_readheader \
           wcc_reformat wcc_resamp_arbdt wcc_resample wcc_resample_send \
           wcc_rms_specavg wcc_rotate wcc_rotphase wcc_scalestf wcc_tapercut \
           wcc_tcut wcc_tfilter wcc_wboost wcc_wfilter wcc_windownorm \
           wcc_durfling wcc_wrescale wcc_zpass winbin2wcc xcor_2comp \
	   lzgf2bgf lzgf2ngf cdmg2wcc wcc_groupvel wcc_degaus wcc_analytic \
	   fit_sine-pulse gen_sine-pulse stack-resid kjartQ wcc2gmt \
	   bgf2wcc bgf2pntsrc fdbin2foam foam2wcc icopy apgf2bgf \
	   wcc2sac_rob apgf2ngf sac2statlist wcc_codeamp resp_codeamp \
	   wcc_siteamp stoch2genrock stoch_siteamp scec2sac bgf_swapbytes bounce_swapbytes \
	   wcc_addrand borch_siteamp test_swapbytes test_fourg test_fourg-2d \
	   wcc_findTp leastsquares wcc_insert_ts wcc_insert_ts-list \
	   bindata2wcc ts2xyz get_tsheader wcc_specmod dump_fdbin \
	   vango_wcc write_tsheader merge_tsP3 merge_ts cs2005_siteamp subgf2wcc cb2006_siteamp \
	   gen_miyatake cb2008_siteamp wcc_saragoni-hart \
	   wcc2bbp respect2bbp bgf_changedep tfilter read_tsheader wcc_genrand wcc_dft \
	   wcc_arias cicese2wcc gen_bounce_inpt bgf_dumpheader respect2bbp_mcomp \
	   bin3float
BIN_LIST = merge_tsP3_par_contiguous 
SERIAL_BIN_LIST = merge_tsP3 merge_tsP3_original read_tsP3
##### compile options

CFLAGS = ${UFLAGS} ${LF_FLAGS}
FFLAGS = ${UFLAGS} -ffixed-line-length-132

##### make options

MAIN_TARG := ${MAKECMDGOALS}
TARG_SRC := ${MAKECMDGOALS}.c

$(BIN_LIST) : ${TARG_SRC} ${COBJS} ${FOBJS}
	${CC} ${CFLAGS} -o $@ $@.c ${LDLIBS}

${COBJS} ${FOBJS} :

$(SERIAL_BIN_LIST) : ${TARG_SRC} ${COBJS} ${FOBJS}
	gcc ${CFLAGS} -o $@ $@.c ${LDLIBS};\
	echo "built!"
#	cp $@ ../;

all :
	for i in $(BIN_LIST); do \
		make $$i; \
#		cp $$i ../; \
	done
	
serial :
	for i in $(SERIAL_BIN_LIST); do \
		make $$i; \
#		cp $$i ../; \
	done

clean:
	rm -f *.o $(BIN_LIST) *~
